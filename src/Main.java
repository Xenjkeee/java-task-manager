import java.io.*;
import java.util.*;

/**
 * Created by Stas on 28.09.2015.
 */
public class Main {
    public static void main(String args[]) throws CloneNotSupportedException, IOException {
        /*
        ArrayTaskList list = new ArrayTaskList();
        for(int i = 0; i < 10; i ++) {
            if(i%2 == 0) {
                list.addTask(new Task("task"+(i+1),new Date(i*1000000)));
            }else {
                list.addTask(new Task("task"+(i+1),new Date(i*1000000),new Date(i*5000000), i*500000));
            }
            list.getTask(i).setActive(true);
        }

        list.addTask(new Task("task1"+(5+1),new Date(6*1000000)));
        list.addTask(new Task("task11"+(5+1),new Date(5*1000000)));
        list.addTask(new Task("task111"+(5+1),new Date(5*1000000)));
        list.addTask(new Task("task1111"+(5+1),new Date(5*1000000)));
        list.addTask(new Task("today Task",new Date(new Date().getTime() +1000*60*60 )));

        ArrayTaskList newList = (ArrayTaskList)Tasks.incoming((ArrayTaskList)list.clone(),new Date(3*1000000),new Date(8*1000000));
        System.out.println("FROM:"+(new Date(3*1000000))+" TO:"+(new Date(8*1000000)));
        System.out.println(newList.toString());
        FileOutputStream fileOutputStream = new FileOutputStream("data.dat");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        TaskIO.write(newList,objectOutputStream);
        File file = new File("data.bin");
        TaskIO.writeBinary(newList,file);
        */
        /*
        File file = new File("data.bin");
        ArrayTaskList arrayTaskList = new ArrayTaskList();
        TaskIO.readBinary(arrayTaskList,file);
        */
        /*
        File file1 = new File("tasks.txt");
        TaskIO.writeText(arrayTaskList,file1);
        */

        File file1 = new File("tasks.txt");
        TaskList taskList = new ArrayTaskList();
        TaskIO.readText(taskList,file1);
        System.out.println(taskList.toString());

        /*
        FileOutputStream fileOutputStream = new FileOutputStream("data.dat");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        TaskIO.write(arrayTaskList,objectOutputStream);
        fileOutputStream.close();
        /*
        FileInputStream fileInputStream = new FileInputStream("data.dat");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ArrayTaskList list = new ArrayTaskList();
        TaskIO.read(list ,objectInputStream);
        System.out.println(list);
        /*
        FileInputStream fileInputStream = new FileInputStream("data.dat");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ArrayTaskList arrayTaskList = new ArrayTaskList();
        TaskIO.read(arrayTaskList,objectInputStream);
        arrayTaskList.toString();
        */
    }
}
