import java.io.Serializable;
import java.util.*;

/**
 * Created by Stas on 08.11.2015.
 */
public class Tasks implements Serializable {
    public static Iterable<Task> incoming (Iterable<Task> tasks, Date start, Date end) {

        Iterator<Task> iterator = tasks.iterator();
        while(iterator.hasNext()) {
            Task current = iterator.next();
            if (!(current .nextTimeAfter(start).getTime() >= start.getTime() && current .nextTimeAfter(start).getTime() <= end.getTime())) {
                iterator.remove();
            }
        }
        return tasks;
    }
    public static SortedMap<Date, Set<Task>> calendar(Iterable<Task> tasks, Date start, Date end){
        SortedMap<Date, Set<Task>> map = new TreeMap ();
        Iterator<Task> iterator = incoming(tasks,start,end).iterator();
        while(iterator.hasNext()) {
            Task current = iterator.next();
            HashSet<Date> dates = (HashSet)current.getAllDates();
            for(Date date : dates){
                if(map.containsKey(date)){
                    HashSet<Task> taskHashSet = (HashSet)map.get(date);
                    taskHashSet.add(current);
                }else{
                    HashSet<Task> newHS = new HashSet<Task>();
                    newHS.add(current);
                    map.put(date,newHS);
                }
            }
        }
        return  map;
    }
}
