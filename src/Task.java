import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

/**
 * Created by Stas on 28.09.2015.
 */
public class Task implements Cloneable, Serializable{

    private String title;

    private boolean active;
    private boolean repeated;

    private Date time;

    private Date start;
    private Date end;
    private int interval;

    private Task (String title) {
        setTitle(title);
        setActive(true);
    }

    public Task(String title, Date time) {
        this(title);
        setTime(time);
    }

    public Task(String title, Date start, Date end, int interval) {
        this(title);
        setTime(start,end,interval);
    }

    public void setTime(Date time) throws NullPointerException {
        if(time == null)
            throw new NullPointerException("Time can't be NULL.");
        this.time = time;
        this.repeated = false;
    }
    public Date getTime() {
        if (isRepeated())
            return this.start;
        return this.time;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setTime(Date start, Date end, int interval) throws NullPointerException{
        if(start == null || end == null || interval <= 0 )
            throw new NullPointerException("Time or repeat interval can't be NULL.");
        this.start = start;
        this.end = end;
        this.interval = interval;
        this.repeated = true;
    }
    public Date getStartTime() {
        if (!isRepeated())
            return time;
        return start;
    }
    public Date getEndTime() {
        if (!isRepeated())
            return time;
        return end;
    }
    public int getRepeatInterval() {
        if (!isRepeated())
            return 0;
        return interval;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
    public boolean isActive() {
        return active;
    }
    public boolean isRepeated() {
        return repeated;
    }

    public Date nextTimeAfter(Date current) {
        if(!isActive())
            return new Date(-1);
        long answer;
        int interval = getRepeatInterval() == 0? 1 : getRepeatInterval();
        for(answer = getStartTime().getTime(); answer <= getEndTime().getTime(); answer += interval){
            if(current.getTime() < answer)
                return new Date(answer);
        }
        return new Date(-1);

    }

    public Set<Date> getAllDates(){
        Set<Date> set = new HashSet();
        if(!isRepeated()) set.add(this.getTime());
        else {
            for(long i = getStartTime().getTime(); i <= getEndTime().getTime(); i += interval){
                set.add(new Date(i));
            }
        }
        return set;
    }

    public boolean equals(Task otherTask) {
        return this.toString().equals(otherTask.toString());
    }
    public int hashCode() {
        return toString().hashCode();
    }
    private String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss.S");
        String result = "["+df.format(date)+"]";
        return result;

    }
    public String toString(){
        StringBuffer  stringBuffer = new StringBuffer();
        stringBuffer.append("\""+getTitle().replaceAll("\"","\"\"") +"\"");
        if(isRepeated()) {
            stringBuffer.append(" from "+dateToString(getStartTime())+" to "+dateToString(getEndTime()));
            long rest = getRepeatInterval();
            int days = (int) rest / (1000*60*60*25);
            rest -= days * 1000*60*60*24;
            int hours  = (int)rest / (1000 * 60 * 60);
            rest -= hours * 1000*60*60 ;
            int minutes = (int)rest / (1000*60);
            rest -= minutes*1000*60;
            int seconds = (int)rest/1000;
            stringBuffer.append(" every [");
            if(days > 0) {
                stringBuffer.append(days+" day"+((days>1)?"s ":" "));
            }
            if(hours > 0) {
                stringBuffer.append(hours+" hour"+((hours>1)?"s ":" "));
            }
            if(minutes > 0) {
                stringBuffer.append(minutes+" minute"+((minutes>1)?"s ":" "));
            }
            if(seconds > 0) {
                stringBuffer.append(seconds+" second"+((seconds>1)?"s ":" "));
            }
            stringBuffer.append("]");
        }else{
            stringBuffer.append(" at "+dateToString(getStartTime()));
        }
        if(!isActive()) {
            stringBuffer.append(" inactive");
        }
        return stringBuffer.toString();
    }
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}