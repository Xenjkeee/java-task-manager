import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by Stas on 28.09.2015.
 */
public abstract class TaskList implements Iterable<Task>, Cloneable, Serializable{
    private int size;

    public Iterator<Task> iterator() {
        return  new Iterator<Task>() {
            private int current = 0;
            @Override
            public boolean hasNext() {
                return (current < getSize());
            }

            @Override
            public Task next() {
                if (! hasNext()) throw new NoSuchElementException();
                return getTask(current++);
            }

            public void remove() {
                removeTask(getTask(--current));
            }

        };
    }

    public TaskList () {
        setSize(0);
    }
    public void setSize(int size) {
        this.size = size;
    }
    public int getSize() {
        return size;
    }

    public void addTask(Task task) throws NullPointerException {
        if (task == null) {
            throw new NullPointerException("task can't be null.");
        }
    }
    public abstract Task getTask(int index) throws  IndexOutOfBoundsException ;

    public void removeTask(Task task){
        setSize(getSize()-1);
    }

    public String toString(){
        StringBuffer buff =  new StringBuffer();
        buff.append("Size:"+getSize()+"\n");
        for(int i = 0 ; i < getSize(); i++){
            buff.append(getTask(i).toString());
            if(i<getSize()-1){
                buff.append(";\n");
            }else{
                buff.append(".\n");
            }
        }
        return buff.toString();
    }
    public int hashCode() {
        return toString().hashCode();
    }
    public boolean equals(TaskList otherList) {
        return toString().equals(otherList.toString());
    }
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
