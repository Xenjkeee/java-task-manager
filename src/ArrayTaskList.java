/**
 * Created by Stas on 30.09.2015.
 */
public class ArrayTaskList extends TaskList {
    private Task[] array;

    public ArrayTaskList() {
        super();
        array = new Task[0];
    }

    @Override
    public void removeTask(Task task) {
        array = cloneArrayWithout(array,find(task));
    }
    public void addTask(Task task)  throws NullPointerException {
        super.addTask(task);
        array = cloneArrayWith(array);
        array[array.length-1] = task;
        setSize(getSize()+1);
    }

    private Task[] cloneArrayWithout(Task[] from, int without ) {
        if(without >=0 && without < getSize())
            super.removeTask(null);
        Task[] to = new Task[from.length -1];
        for(int i=0; i<to.length; i++){
            if(i < without){
                to[i] = from[i];
                continue;
            }
            if(i>=without)
                to[i] = from[i+1];
        }
        return to;
    }

    private int find(Task task) {
        for(int i = 0; i < array.length; i++){
            if(array[i].equals(task))
                return i;
        }
        return -1;
    }

    private Task[] cloneArrayWith(Task[] from) {
        Task[] to = new Task[from.length +1];
        for(int i=0; i<from.length; i++)
                to[i] = from[i];
        return to;
    }

    public Task getTask(int index) throws IndexOutOfBoundsException {
        try {
            return array[index];
        } catch (ArrayIndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("bad index");
        }
    }
}
