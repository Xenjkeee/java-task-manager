import java.util.Iterator;

/**
 * Created by Stas on 30.09.2015.
 */
public class LinkedTaskList extends TaskList {

    public class Node {
        private Task data;
        private Node next;

        public Node(Task task) {
            setData(task);
        }
        public Task getData(){
            return data;
        }
        public void setData(Task task){
            this.data = task;
        }
        public Node getNext() {
            return next;
        }
        public void setNext(Node node) {
            next = node;
        }
        public boolean hasNext(){
            return getNext() != null ? true : false;
        }
    }

    private Node firstNode;

    public LinkedTaskList() {
        super();
        firstNode = null;
    }

    @Override
    public void removeTask(Task task) {
        Node current = firstNode;
        if(firstNode.getData().equals(task)) {
            firstNode = firstNode.getNext();
            super.removeTask(task);
            return;
        }
        while(current.hasNext()){
            if(current.getNext().getData().equals(task)) {
                current.setNext(current.getNext().getNext());
                super.removeTask(task);
                return;
            }
            current = current.getNext();
        }


    }
    public void addTask(Task task) throws NullPointerException{
        super.addTask(task);
        setSize(getSize()+1);
        if(firstNode == null) {
            firstNode = new Node(task);
            return;
        }
        Node current = firstNode;
        while(current.hasNext())
            current = current.getNext();
        current.setNext(new Node(task));
    }

    public Task getTask(int index) throws IndexOutOfBoundsException {
        if(index > getSize())
            throw new IndexOutOfBoundsException("bad index");
        Node current = firstNode;
        for(int i = 0; i < index; i++)
            current = current.getNext();
        return current.getData();
    }
}
