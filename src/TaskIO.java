import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Stas on 11.12.2015.
 */
public  class TaskIO {
    private static Date StringToDate(String time) {
        DateFormat df = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss.S");
        try {
            return df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void write(TaskList tasks, OutputStream out) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(out);
            oos.writeObject(tasks);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void  read(TaskList tasks, InputStream in) {
        try {
            ObjectInputStream ois = new ObjectInputStream(in);
            ArrayTaskList arrayTaskList = (ArrayTaskList)ois.readObject();
            for(Task task : arrayTaskList){
                tasks.addTask(task);
            }
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
    public static void writeBinary(TaskList tasks, File file) {
        try {
            DataOutputStream dataOutput = new DataOutputStream(new FileOutputStream(file));
            dataOutput.writeInt(tasks.getSize());
            for(Task task: tasks) {
                dataOutput.writeInt(task.getTitle().length());
                dataOutput.writeChars(task.getTitle());
                dataOutput.writeBoolean(task.isActive());
                dataOutput.writeInt(task.isRepeated()?task.getRepeatInterval():0);
                if(task.isRepeated()) {
                    dataOutput.writeLong(task.getStartTime().getTime());
                    dataOutput.writeLong(task.getEndTime().getTime());
                }else {
                    dataOutput.writeLong(task.getTime().getTime());
                }
            }
            dataOutput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void readBinary(TaskList tasks, File file) {
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
            int size = dataInputStream.readInt();
            for(int i = 0; i < size; i++){
                int length = dataInputStream.readInt();
                StringBuffer title = new StringBuffer();
                for(int j = 0; j < length; j++)
                    title.append(dataInputStream.readChar());
                boolean active = dataInputStream.readBoolean();
                int interval = dataInputStream.readInt();
                if(interval == 0){
                    tasks.addTask(new Task(title.toString(),new Date(dataInputStream.readLong())));
                }else{
                    tasks.addTask(new Task(title.toString(),
                            new Date(dataInputStream.readLong()),
                            new Date(dataInputStream.readLong()),interval)
                    );
                }
            }
            dataInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void write(TaskList tasks, Writer out) {
        BufferedWriter bufferedWriter = new BufferedWriter(out);
        try {
            bufferedWriter.write(tasks.toString());
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void writeText(TaskList tasks, File file) {
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(tasks.toString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private static Task parse(String string) {
        int first = string.indexOf("\"");
        int last = string.lastIndexOf("\"");
        String title = string.substring(first+1,last);
        if(string.indexOf("every") > 0){
            String buff = string.substring(string.indexOf("from"),string.indexOf("to"));
            Date from = StringToDate(buff.substring(buff.indexOf('[')+1,buff.indexOf(']')));
            buff = string.substring(string.indexOf("to"),string.indexOf("every"));
            Date to = StringToDate(buff.substring(buff.indexOf('[')+1,buff.indexOf(']')));
            buff = string.substring(string.indexOf("every"),string.lastIndexOf("]"));
            int interval = stringToInterval(buff);
            return new Task(title,from,to,interval);
        }
        String time = string.substring(string.indexOf('[')+1,string.indexOf(']'));
        Date startTime = new Date();
        startTime= StringToDate(time);

        return new Task(title,startTime);
    }
    private static int stringToInterval(String string) {
        String str = string.replaceAll("[^0-9]+", " ");
        String[] list = str.trim().split(" ");
        List<String> array = new ArrayList<>();
        for(String s : list){
            array.add(s);
        }
        int result = 0;
        if(string.indexOf("day") >= 0) {
            result += Integer.parseInt(array.get(0))*1000 * 60 * 60 * 24;
            array.remove(0);
        }
        if(string.indexOf("hour") >= 0) {
            result += Integer.parseInt(array.get(0))*1000 * 60 * 60;
            array.remove(0);
        }
        if(string.indexOf("minute") >= 0) {
            result += Integer.parseInt(array.get(0))*1000 * 60 ;
            array.remove(0);
        }
        if(string.indexOf("second") >= 0) {
            result += Integer.parseInt(array.get(0))*1000 ;
            array.remove(0);
        }
        return result;
    }
    public static void read(TaskList tasks, Reader in) {
        BufferedReader bufferedReader = new BufferedReader(in);
        try {
            String header = bufferedReader.readLine();
            int size = Integer.parseInt(header.substring(header.indexOf(':')+1));
            for(int i = 0; i < size; i++) {
                tasks.addTask(parse(bufferedReader.readLine()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void readText(TaskList tasks, File file) {
        FileReader reader = null;
        try {
            reader = new FileReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(reader);
        try {
            String header = bufferedReader.readLine();
            int size = Integer.parseInt(header.substring(header.indexOf(':') + 1));
            for(int i = 0; i < size; i++) {
                tasks.addTask(parse(bufferedReader.readLine()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
